﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryService.Models;

namespace InventoryService.Service
{
    public class InventoryService : IiventoryService
    {
        private readonly Dictionary<string, InventoryItems> _inventoryItem;

        public InventoryService()
        {
            _inventoryItem = new Dictionary<string, InventoryItems>();
        }
        public InventoryItems AddInventoryItems(InventoryItems items)
        {
            _inventoryItem.Add(items.ItemName, items);
            return items;
        }
         
        public Dictionary<string, InventoryItems> GetInventoryItems()
        {
            return _inventoryItem;
        }
    }
}
